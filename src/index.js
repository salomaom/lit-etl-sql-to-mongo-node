// dotenv configuration start

import dotenv from "dotenv";
const result = dotenv.config();
if (result.error) {
  throw result.error;
}

// dotenv configuration end

import { MongoClient } from "mongodb";
import sql from "mssql";

const sqlConfig = {
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  server: process.env.DATABASE_HOST,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  options: {
    encrypt: false, // for azure
    trustServerCertificate: true, // change to true for local dev / self-signed certs
  },
};

const getResultSQL = async () => {
  try {
    // make sure that any items are correctly URL encoded in the connection string
    await sql.connect(sqlConfig);
    const result = await sql.query`select top 10 * from progress`;
    console.dir(result);
    console.log("END");
  } catch (err) {
    console.log(err);
  }
};

await getResultSQL();

// const mongoClient = new MongoClient(process.env.MONGO_CONNECTION);
// const getResultMongo = async () => {
//   try {
//     await mongoClient.connect();

//     const db = mongoClient.db("lit-ms-progress-node");
//     const progressCollection = db.collection("progresses");

//     const progressesCursor = progressCollection.find();

//     await progressesCursor.forEach(console.dir);

//     console.log("END");
//   } catch (error) {
//     console.log(error);
//   } finally {
//     await mongoClient.close();
//   }
// };

// await getResultMongo();
