import axios from "axios";

export default class ProgressService {
    constructor() {
      this._progressAPI = axios.create({
        baseURL: process.env.MS_PROGRESS_URL,
      });
    }

    async saveProgress(progress = {}, owner, ownerIdProfile) {
      try {
        const response = await this._progressAPI.post(
          '/progress/saveProgress',
          progress,
          {
            headers: {
              'x-owner': owner,
              'x-owner-idprofile': ownerIdProfile,
            },
          }
        );
  
        return response.data;
      } catch (error) {
        console.log(error);
        throw new Error(
          'Não foi possível salvar o progresso do usuário',
          400
        );
      }
    }
  
    async saveSelfAssessmentProgress(progress = {}, owner, ownerIdProfile) {
      try {
        const response = await this._progressAPI.post(
          '/progress/selfAssessment',
          progress,
          {
            headers: {
              'x-owner': owner,
              'x-owner-idprofile': ownerIdProfile,
            },
          }
        );
  
        return response.data;
      } catch (error) {
        console.log(error);
        throw new Error(
          'Não foi possível salvar o progresso do usuário',
          400
        );
      }
    }

  }
  

  